# Sapura assessment spring boot

- Spring Boot version 3.2.3
- Java openJDK 21

## Database MYSQL

CREATE DATABASE `sapura_timesheet`

### User & Status tables autocreate

Once the application is running, these table will be auto generate for the purpose of this assessment. See `application.properties` file on `DDL-auto`

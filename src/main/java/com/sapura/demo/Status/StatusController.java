package com.sapura.demo.Status;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/v1/status")
@CrossOrigin(origins = "http://localhost:4200")
public class StatusController {
  private final StatusService statusService;

  public StatusController(StatusService statusService) {
    this.statusService = statusService;
  }

  @GetMapping
  public List<Status> getStatus() {
    return statusService.getStatus();
  }

}

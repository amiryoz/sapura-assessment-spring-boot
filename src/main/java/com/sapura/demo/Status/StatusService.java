package com.sapura.demo.Status;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StatusService {
  private final StatusRepository statusRepository;

  public StatusService(StatusRepository statusRepository) {
    this.statusRepository = statusRepository;
  }

  public List<Status> getStatus() {
    return statusRepository.findAll();
  }
}

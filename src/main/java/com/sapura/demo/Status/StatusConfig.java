package com.sapura.demo.Status;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StatusConfig {

  @Bean
  CommandLineRunner clrStatus(StatusRepository statusRepository) {
    return args -> {
      Status status1 = new Status(
          "Open", "task that are not yet owned");

      Status status2 = new Status(
          "In Progress", "task that is being worked on");

      Status status3 = new Status(
          "Closed", "task that has been completed");

      statusRepository.saveAll(List.of(status1, status2, status3));
    };
  }
}

package com.sapura.demo.Status;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sapura.demo.Timesheet.Timesheet;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity(name = "status")
@Table(name = "status")
public class Status {

  @Id
  @SequenceGenerator(name = "status_sq", sequenceName = "status_sq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "status_sq")
  @Column(name = "id", updatable = false)
  private Integer id;
  @Column(name = "status")
  private String status;
  @Column(name = "description")
  private String description;

  @JsonIgnore
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "status")
  private List<Timesheet> timesheets;

  public Status() {
  }

  public Status(String status, String description) {
    this.status = status;
    this.description = description;
  }

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getStatus() {
    return this.status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<Timesheet> getTimesheets() {
    return this.timesheets;
  }

  public void setTimesheets(List<Timesheet> timesheets) {
    this.timesheets = timesheets;
  }

  @Override
  public String toString() {
    return "{" +
        " id='" + getId() + "'" +
        ", status='" + getStatus() + "'" +
        ", description='" + getDescription() + "'" +
        "}";
  }

}

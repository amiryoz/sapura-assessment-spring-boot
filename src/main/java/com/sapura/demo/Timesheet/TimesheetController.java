package com.sapura.demo.Timesheet;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@ResponseBody
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "api/v1/timesheet")
public class TimesheetController {
  private final TimesheetService timesheetService;

  public TimesheetController(TimesheetService timesheetService) {
    this.timesheetService = timesheetService;
  }

  @GetMapping
  public List<Timesheet> getTimesheets() {
    return timesheetService.getTimesheets();
  }

  @GetMapping("/findTimesheet")
  public List<Timesheet> getTimesheetsWithQuery(@RequestParam String query) {
    System.out.println(query);
    return timesheetService.getTimesheetWithQuery(query);
  }

  @PostMapping
  public void createTimesheet(@RequestBody Timesheet timesheet, @RequestParam Long userId,
      @RequestParam Integer statusId) {
    timesheetService.saveTimesheet(timesheet, userId, statusId);
  }

  @PutMapping("/update/{timesheetId}")
  public void updateTimesheet(@RequestBody Timesheet timesheet, @PathVariable Long timesheetId,
      @RequestParam Long userId, @RequestParam Integer statusId) {

    timesheetService.updateTimesheet(timesheet, timesheetId, userId, statusId);
  }

  @DeleteMapping(path = "/delete/{timesheetId}")
  public void removeTimesheet(@PathVariable("timesheetId") Long id) {
    timesheetService.deleteTimesheet(id);
  }
}

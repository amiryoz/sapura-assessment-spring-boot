package com.sapura.demo.Timesheet;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import com.sapura.demo.Status.Status;
import com.sapura.demo.Status.StatusRepository;
import com.sapura.demo.User.User;
import com.sapura.demo.User.UserRepository;

@Service
public class TimesheetService {
  private final TimesheetRepository timesheetRepository;
  private final UserRepository userRepository;
  private final StatusRepository statusRepository;

  public TimesheetService(TimesheetRepository timesheetRepository, UserRepository userRepository,
      StatusRepository statusRepository) {
    this.timesheetRepository = timesheetRepository;
    this.userRepository = userRepository;
    this.statusRepository = statusRepository;
  }

  @GetMapping
  public List<Timesheet> getTimesheets() {
    return timesheetRepository.findAll();
  }

  @GetMapping
  public List<Timesheet> getTimesheetWithQuery(String query) {
    return timesheetRepository.findTimesheetsByQuery(query);
  }

  @PostMapping
  public void saveTimesheet(Timesheet timesheet, Long userId, Integer statusId) {
    Optional<User> userOpt = userRepository.findById(userId);
    Optional<Status> statusOpt = statusRepository.findById(statusId);

    try {
      if (!userOpt.isPresent()) {
        throw new IllegalThreadStateException("User not found");
      }

      if (!statusOpt.isPresent()) {
        throw new IllegalThreadStateException("Status not found");
      }

      timesheet.setStatus(statusOpt.get());
      timesheet.setUser(userOpt.get());
      timesheetRepository.save(timesheet);
    } catch (Exception e) {
      System.out.println("User not found on path");
    }

  }

  @PutMapping()
  public void updateTimesheet(Timesheet timesheet, Long timesheetId, Long userId, Integer statusId) {

    Optional<Timesheet> timesheetsOpt = timesheetRepository.findById(timesheetId);
    Optional<User> userOpt = userRepository.findById(userId);
    Optional<Status> statusOpt = statusRepository.findById(statusId);
    try {

      if (!timesheetsOpt.isPresent()) {
        throw new IllegalThreadStateException("Timesheet not found");
      }

      if (!userOpt.isPresent()) {
        throw new IllegalThreadStateException("User not found");
      }

      if (!statusOpt.isPresent()) {
        throw new IllegalThreadStateException("Status not found");
      }

      Timesheet newUpdate = timesheetsOpt.get();
      User newUser = userOpt.get();
      Status newStatus = statusOpt.get();

      newUpdate.setStatus(newStatus);
      newUpdate.setUser(newUser);
      newUpdate.setProjectName(timesheet.getProjectName());
      newUpdate.setTask(timesheet.getTask());
      timesheetRepository.save(newUpdate);

    } catch (Exception e) {
      System.out.println("User not found on path");
    }

  }

  @DeleteMapping
  public void deleteTimesheet(Long timesheetId) {
    boolean exists = timesheetRepository.existsById(timesheetId);

    try {
      if (!exists) {
        throw new IllegalStateException("Timesheet ID not found");
      }
    } catch (Exception e) {
      System.out.println("Timesheet ID not found on path");
    }

    timesheetRepository.deleteById(timesheetId);
  }

}

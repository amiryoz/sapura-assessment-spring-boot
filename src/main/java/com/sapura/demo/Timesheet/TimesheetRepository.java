package com.sapura.demo.Timesheet;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TimesheetRepository extends JpaRepository<Timesheet, Long> {

  @Query(value = "select * from timesheet where task like %:q%", nativeQuery = true)
  public List<Timesheet> findTimesheetsByQuery(@Param("q") String query);

}

package com.sapura.demo.Timesheet;

import java.time.LocalDate;

import com.sapura.demo.Status.Status;
import com.sapura.demo.User.User;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;

@Entity(name = "Timesheet")
public class Timesheet {
  @Id
  @SequenceGenerator(name = "timesheet_sq", sequenceName = "timesheet_sq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "timesheet_sq")
  @Column(name = "id", updatable = false)
  private Integer id;
  @Column(name = "projectName", nullable = false, columnDefinition = "TEXT")
  private String projectName;
  @Column(name = "task", nullable = false, columnDefinition = "TEXT")
  private String task;
  @Column(name = "date_from", nullable = true)
  private LocalDate date_from;
  @Column(name = "date_to", nullable = true)
  private LocalDate date_to;

  @ManyToOne()
  @JoinColumn(name = "status_id", referencedColumnName = "id")
  private Status status;

  @ManyToOne()
  @JoinColumn(name = "user_id", referencedColumnName = "id")
  private User user;

  public Timesheet() {
  }

  public Timesheet(String projectName, String task, LocalDate date_from, LocalDate date_to) {
    this.projectName = projectName;
    this.task = task;
    this.date_from = date_from;
    this.date_to = date_to;
  }

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getProjectName() {
    return this.projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  public String getTask() {
    return this.task;
  }

  public void setTask(String task) {
    this.task = task;
  }

  public LocalDate getDate_from() {
    return this.date_from;
  }

  public void setDate_from(LocalDate date_from) {
    this.date_from = date_from;
  }

  public LocalDate getDate_to() {
    return this.date_to;
  }

  public void setDate_to(LocalDate date_to) {
    this.date_to = date_to;
  }

  public User getUser() {
    return this.user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Status getStatus() {
    return this.status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "Timesheet{" +
        " id='" + getId() + "'" +
        ", projectName='" + getProjectName() + "'" +
        ", task='" + getTask() + "'" +
        ", date_from='" + getDate_from() + "'" +
        ", date_to='" + getDate_to() + "'" +
        "}";
  }

}

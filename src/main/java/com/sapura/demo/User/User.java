package com.sapura.demo.User;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sapura.demo.Timesheet.Timesheet;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.GenerationType;
import jakarta.persistence.UniqueConstraint;

@Entity(name = "users")
@Table(name = "users", uniqueConstraints = { @UniqueConstraint(name = "user_email_unique", columnNames = "email") })
public class User {

  @Id
  @SequenceGenerator(name = "user_sq", sequenceName = "user_sq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_sq")
  @Column(name = "id", updatable = false)
  private Long id;
  @Column(name = "name", nullable = false, columnDefinition = "TEXT")
  private String name;
  @Column(name = "email", nullable = false)
  private String email;

  @JsonIgnore
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
  private List<Timesheet> timesheets;

  public User(String name, String email) {
    this.name = name;
    this.email = email;
  }

  public User() {
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public List<Timesheet> getTimesheets() {
    return this.timesheets;
  }

  public void setTimesheets(List<Timesheet> timesheets) {
    this.timesheets = timesheets;
  }

  @Override
  public String toString() {
    return "User{" +
        " id='" + getId() + "'" +
        ", name='" + getName() + "'" +
        ", email='" + getEmail() + "'" +
        "}";
  }

}

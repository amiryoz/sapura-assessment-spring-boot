package com.sapura.demo.User;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class UserConfig {

  @Bean
  CommandLineRunner clrUser(UserRepository ur) {
    return args -> {
      User user1 = new User(
          "Amir",
          "amir@gmail.com");

      User user2 = new User(
          "Nurul",
          "nurul@gmail.com");

      User user3 = new User(
          "Jasmine",
          "jasmine@gmail.com");

      ur.saveAll(List.of(user1, user2, user3));
    };

  }

}
